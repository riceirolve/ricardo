/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import org.junit.Test;
import static org.junit.Assert.*;

public class TASK1Test {

    
    @Test
    public void testIfIsPalindromee() {
        TASK1 task = new TASK1();
        String word = "";
        
        word = "racecar";
        assertTrue("It is",task.isPalindrome(word));
        
        word = "madam";
        assertTrue("It is",task.isPalindrome(word));
        
        word = "rome";
        assertFalse("It is not",task.isPalindrome(word));
        
        word = "amora";
        assertFalse("It is not",task.isPalindrome(word));
    }
    
}
