package com.example;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.myinterview.connection.DBConnection;

public class FuncionarioDAO {

	DBConnection dbConnection = new DBConnection();

	public FuncionarioDAO() {
	}

	public void quantidadeFuncionariosPorSexo() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			connection = dbConnection.getConnection();
			pstmt = connection.prepareStatement("SELECT COUNT(FuncionarioId), Sexo FROM Funcionario GROUP BY Sexo");
			rs = pstmt.executeQuery();
			if (rs.next())
				System.out.println(rs.getInt(0) + " " + rs.getString(1));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			dbConnection.close(connection);
			dbConnection.close(rs);
			dbConnection.close(pstmt);
		}
	}

	public void quantidadeFuncionariosPorSexoENascimento() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			connection = dbConnection.getConnection();
			pstmt = connection.prepareStatement(
					"SELECT COUNT(FuncionarioId), Sexo, AnoNascimento FROM Funcionario GROUP BY Sexo, AnoNascimento");
			rs = pstmt.executeQuery();
			if (rs.next())
				System.out.println(rs.getInt(0) + " " + rs.getString(1) + " " + rs.getString(2));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			dbConnection.close(connection);
			dbConnection.close(rs);
			dbConnection.close(pstmt);
		}
	}
	
	
	public void mediaMinMaxSalarioPorSexo() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			connection = dbConnection.getConnection();
			pstmt = connection.prepareStatement(
					"SELECT min(salario) as minimo, max(salario) as maximo,  avg(salario) as media, sexo FROM Funcionario GROUP BY Sexo");
			rs = pstmt.executeQuery();
			if (rs.next())
				System.out.println("Minimo: " + rs.getDouble(0) + " Máximo: " + rs.getDouble(1) + " Média: " + rs.getDouble(2) + " " + rs.getString(3));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			dbConnection.close(connection);
			dbConnection.close(rs);
			dbConnection.close(pstmt);
		}
	}

}
