package com.example;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {

	public boolean isPalindrome(String word) {
		String drow = "";
		for (int i = word.length(); i > 0; i--)
			drow += word.substring(i - 1, i);
		return word.equals(drow);
	}

}
